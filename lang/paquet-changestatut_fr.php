<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-changestatut
// Langue: fr
// Date: 23-01-2012 10:49:28
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'changestatut_description' => 'Changer temporairement et d\'un clic son statut de webmestre en administrateur ou rédacteur. Puis revenir à la situation originale.',
	'changestatut_slogan' => 'Modifier, le temps d\'un test, son statut de webmestre',
);
?>