<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'versadmin' => 'Changer le statut à administrateur',
	'versredacteur' => 'Changer le statut à rédacteur',
	'verswebmestre' => 'Changer le statut à webmestre'
);
?>
